#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <math.h> 

static const int64_t COIN = 100000000;

double ConvertBitsToDouble(unsigned int nBits)
{
    int nShift = (nBits >> 24) & 0xff;

    double dDiff =
        (double)0x0000ffff / (double)(nBits & 0x00ffffff);

    while (nShift < 29)
    {
        dDiff *= 256.0;
        nShift++;
    }
    while (nShift > 29)
    {
        dDiff /= 256.0;
        nShift--;
    }

    return dDiff;
}

int64_t static GetBlockBaseValue(int nBits, int nHeight)
{
    double dDiff =
        (double)0x0000ffff / (double)(nBits & 0x00ffffff);

    /* fixed bug caused diff to not be correctly calculated */
    if(nHeight > 4500) dDiff = ConvertBitsToDouble(nBits);

    int64_t nSubsidy = 0; 
    // 10000000/(((x/2+1000)/10)^3)
    nSubsidy = (10000000.0 / (pow((dDiff/2+1000.0)/10.0,3.0))) + 0.5;
            if (nSubsidy > 10) nSubsidy = 10;
	    if (nSubsidy < 1) nSubsidy = 1;
    // printf("height %u diff %4.2f reward %i \n", nHeight, dDiff, nSubsidy);
    nSubsidy *= COIN;

    // monthly decline of production by 0.5% per month (30 days, 43200 blocks), projected 86.4M coins.
    for(int i = 43200; i <= nHeight; i += 43200) nSubsidy *= 0.995;
    if(nHeight == 1) nSubsidy = 432000 * COIN;
    return nSubsidy;
}


#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
using namespace boost::python;
 
BOOST_PYTHON_MODULE(twilightcoin_subsidy)
{
    def("GetBlockBaseValue", GetBlockBaseValue);
}

