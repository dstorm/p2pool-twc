from distutils.core import setup
from distutils.extension import Extension

setup(name="twilightcoin_subsidys",
    ext_modules=[
        Extension("twilightcoin_subsidy", ["twilightcoin_GetBlockBaseValue.cpp"],
        libraries = ["boost_python"])
    ])
